/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/07 15:15:12 by ollevyts          #+#    #+#             */
/*   Updated: 2017/12/07 15:15:14 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static t_line	*aval_line(t_line *lin, int fd)
{
	while (lin->next && lin->fd != fd)
	{
		lin = lin->next;
		if (lin->fd == fd)
			return (lin);
	}
	if (lin->fd != fd)
	{
		lin->next = (t_line*)malloc(sizeof(t_line));
		lin->next->fd = fd;
		lin->next->str = ft_strnew(0);
		lin->next->aft = NULL;
		lin->next->next = NULL;
		lin = lin->next;
	}
	return (lin);
}

static int		ft_gnl_finish(t_line *lin, char **line)
{
	char	*pnt;

	if (lin->aft)
		lin->str = lin->aft;
	pnt = ft_strchr(lin->str, 10);
	if (pnt)
	{
		*pnt = 0;
		*line = ft_strdup(lin->str);
		lin->aft = ft_strdup(pnt + 1);
		return (1);
	}
	if (*lin->str)
	{
		*line = ft_strdup(lin->str);
		lin->aft = lin->str;
		*lin->aft = 0;
		return (1);
	}
	return (0);
}

int				get_next_line(const int fd, char **line)
{
	static	t_line	*lin;
	t_line			*temp;
	char			tmp[BUFF_SIZE + 1];
	int				len;
	char			*buf;

	if (fd < 0 || line == NULL || BUFF_SIZE <= 0 || read(fd, 0, 0) < 0)
		return (-1);
	if (!lin)
	{
		lin = (t_line*)malloc(sizeof(t_line));
		lin->fd = fd;
		lin->str = ft_strnew(0);
		lin->aft = NULL;
		lin->next = NULL;
	}
	temp = aval_line(lin, fd);
	while ((len = read(fd, tmp, BUFF_SIZE)) > 0)
	{
		tmp[len] = 0;
		buf = ft_strjoin(temp->str, tmp);
		free(temp->str);
		temp->str = buf;
	}
	return (ft_gnl_finish(temp, line));
}
