/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/07 15:15:12 by ollevyts          #+#    #+#             */
/*   Updated: 2017/12/07 15:15:14 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line1.h"

static t_line	*get_fd(t_line *8lin, int fd)
{
	t_line	*temp;

	temp = *lin;
	while (temp)
	{
		if (temp->fd == fd)
		{
			if (temp->str == 0)
				temp->str == ft_strnew(0);
			return (temp);
		}
		temp = temp->next;
	}
	if (!(temp = malloc(sizeof(*temp))))
		return (NULL);
	temp->fd = fd;
	temp->str = ft_strnew(0);
	temp->next = *lin;
	*lin = temp;
	return (temp);	
}

static int		ft_gnl(t_line *temp, char **line)
{
	char	*pnt;
	char	*buf;

	if (*(temp->str))
	{
		if (!(pnt = ft_strchr(temp->str, 10)))
		{
			*line = temp->str;
			temp->str = NULL;
		}
		else
		{
			*line = ft_strnew(ptr - temp->str);
			ft_memcpy(*line, temp->str, ptr - temp->str);
			buf = temp->str;
			temp->str = ft_strdup(++ptr);
			ft_strdel(&tmp);
		}
		return (1);
	}
	return (0);
}

int			ft_reading(int fd, t_line *temp)
	{
		char	buf[BUFF_SIZE + 1];
		char	*buffer;
		int		ret;

		while ((ret = read(fd, tmp, BUFF_SIZE)) > 0)
		{
			tmp[ret] = 0;
			buffer = temp->str;
			ft_strdel(&buffer);
			if (ft_strchr(temp->str, 10))
				break ;
		}
		if (ret == -1)
			return (-1);
		if (!(ft_strlen(temp->str)))
			return (0);
		return (1);
	}

int				get_next_line(const int fd, char **line)
{
	static	t_line	*lin;
	t_line			*temp;
	int				read;

	if (fd < 0 || line == NULL|| read(fd, 0, 0) < 0)
		return (-1);
	temp = get_fd(&lin, fd);
	if (!(ft_strchr(temp->str, 10)))
	{
		read = ft_reading(fd, temp)
		if (read == -1)
			return (-1);
		if (read == 0)
			return (0);
	}
	return ((ft_gnl_finish(temp, line)) > 0 ? 1 : 0);
}
