/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <ollevyts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 20:20:11 by ollevyts          #+#    #+#             */
/*   Updated: 2017/11/03 21:23:49 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_itoa(int n)
{
	int			i;
	int			j;
	char		*res;
	long long	c;

	j = 0;
	i = ft_count(n);
	c = (long long)n;
	res = (char *)malloc(sizeof(char) * i + 1);
	if (res == NULL)
		return (NULL);
	res[i] = '\0';
	if (c < 0)
	{
		res[j++] = '-';
		c *= -1;
	}
	while (i > j)
	{
		res[i - 1] = ((c % 10) + '0');
		c /= 10;
		i--;
	}
	return (res);
}
