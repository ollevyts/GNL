/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 20:20:19 by ollevyts          #+#    #+#             */
/*   Updated: 2017/11/02 20:20:21 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int					ft_atoi(const char *str)
{
	unsigned long long	ret;
	int					i;
	int					flag;

	i = 0;
	ret = 0;
	flag = 1;
	while ((str[i] >= 9 && str[i] <= 14) || str[i] == 32)
		i++;
	if (str[i] == '-' || str[i] == '+')
	{
		flag = (str[i] == '-') ? -1 : 1;
		i++;
	}
	while (str[i] > 47 && str[i] < 58)
	{
		if (ret * 10 + (str[i] - '0') < ret)
			return ((flag == 1) ? -1 : 0);
		ret = (ret * 10 + (str[i] - '0'));
		i++;
	}
	return ((int)(ret * flag));
}
