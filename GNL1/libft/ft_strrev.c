/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/14 14:47:05 by ollevyts          #+#    #+#             */
/*   Updated: 2017/11/14 14:47:24 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrev(char *str)
{
	int		i;
	int		len;
	char	temp;

	i = 0;
	if (str == NULL)
		return (NULL);
	len = ft_strlen(str);
	while ((len - 1) > i)
	{
		temp = str[len - 1];
		str[len - 1] = str[i];
		str[i] = temp;
		len--;
		i++;
	}
	return (str);
}
