/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <ollevyts@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 20:19:47 by ollevyts          #+#    #+#             */
/*   Updated: 2017/11/03 21:55:54 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr(int n)
{
	long long z;

	z = (long long)n;
	if (z < 0)
	{
		ft_putchar('-');
		z = -z;
	}
	if (z > 9)
	{
		ft_putnbr(z / 10);
		ft_putnbr(z % 10);
	}
	if (z < 10)
		ft_putchar(z + '0');
}
