/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ollevyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/11 07:16:21 by ollevyts          #+#    #+#             */
/*   Updated: 2017/11/11 07:16:23 by ollevyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*new;

	if (lst == NULL || f == NULL)
		return (NULL);
	if (!(new = (t_list*)malloc(sizeof(*new))))
		return (NULL);
	if (lst->next == NULL)
	{
		new = (*f)(lst);
		return (new);
	}
	new = (*f)(lst);
	new->next = ft_lstmap(lst->next, f);
	return (new);
}
